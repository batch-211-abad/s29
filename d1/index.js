//greater than: "$gt"

db.users.find({age: {$gt: 76}});

//greater than equal to: "$gte:

db.users.find({age: {$gte: 76}});

//less than: $"lt"

db.users.find({age: {$lt: 65}});

//less than or equal to: "$lte"

db.users.find({age: {$lte: 65}});

//not equal to: "$ne"

db.users.find({age: {$ne: 65}});

//in operator: "$in"

db.users.find({lastName: {in: ["Hawking", "Doe"]}});
db.users.find({courses: {in: ["HTML", "React"]}});

//Regex Operator: "$regex"

	//Case Sensivity Query
	db.users.find({lastName: {$regex: 'A'}});

	//Case Insensitive Query
	db.users.find({lastName: {$regex: 'A', $options: '$i'}});

//logical query operator

	//or operator

	db.users.find({$or: [{firstName: "Neil"}, {age: "25"}]});
	db.users.find({$or: [{firstName: "Neil"}, {age: {$gt: 30}}]});

	//and operator

	db.users.find({$and: [{age: {$ne: 82}}, {age: {$ne: 76}}]});

//Field Projection

// Includion

db.users.find(
	{ 
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		"contact.phone": 1
	}
);

db.users.find(
	{ 
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		contact: 1,
		_id: 0
	}
);




//slice operator :$slice

db.users.insert({
	namearr: [
	{
		namea: "Juan"
	},
	{
		nameb: "Tamad"
	}
]
});

db.users.find({
	namearr: 
	{
		namea: "Juan"
	}
});

//using slice operator

db.users.find(
	{"namearr":
		{
			namea: "Juan"
		},
		{
			namearr:
			{$slice: 1}
		}
});


db.users.find(
			{ "namearr": 
				{ 
					namea: "Juan" 
				} 
			}, 
			{ namearr: 
				{ $slice: 1 } 
			}
		)


//Exclusion

db.users.find(
{
	firstName: "Jane"
},
{
	contact: 0,
	department: 0
});

// excluding or suppressing specific fields in embedded document

db.users.find(
{
	firstName: "Jane"
},
{
	"contact.phone": 0
});