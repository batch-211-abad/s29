// Find users with letter s in their first name or d in their last name
// Use the $or operator.
// Show only the firstName and lastName fields and hide the _id field

db.users.find({$or: [{lastName: {$regex: 'd', $options: '$i'}}, {firstName: {$regex: 's', $options: '$i'}}]},
	{
		firstName: 1,
		lastName: 1,
		_id: 0
	});

// Find Users who are from the HR department AND their age is greater than or equal to 70.

db.users.find({$and: [{department: "HR"}, {age: {$gte: 70}}]});

// Find Users with the letter e in their first name and has an age of less than or equal to 30. 

db.users.find({$and: [{age: {$lte: 30}}, {firstName: {$regex: 'e', $options: '$i'}}]});